<!DOCTYPE html>
<html>
    <head>
        <title>Sign Up</title>
    </head>
    <body>
        <h2>Buat Account Baru</h2>
        <h4>Sign Up Form</h4>
        <form action="/welcome" method="post">
            @csrf
            <label>First name:</label><br><br>
            <input type="text" name="first_name">
            <br><br>
            <label>Last name:</label><br><br>
            <input type="text" name="last_name">
            <br><br>
            <label>Gender</label><br><br>
            <input type="radio" name="gender">Male<br>
            <input type="radio" name="gender">Female
            <br><br>
            <label>Nationality</label><br><br>
            <select name="nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="negara lain">Other</option>
            </select>
            <br><br>
            <label>Language Spoken</label><br><br>
            <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
            <input type="checkbox" name="bahasa">English<br>
            <input type="checkbox" name="bahasa">Other
            <br><br>
            <label>Bio</label><br><br>
            <textarea name="bio" rows="10" cols="30"></textarea>
            <br><br>
            <input type="submit" value="Sign Up">
        </form>

    </body>
</html>