<?php
    require_once("animal.php");
    //Release 0
    $sheep = new Animal("shaun");

    echo "Name : " . $sheep->name ."<br>"; // "shaun"
    echo "legs : " . $sheep->legs ."<br>"; // 4
    echo "cold blooded : " . $sheep->cold_blooded ."<br>"; // "no"

    echo "<br>";

    //Release 1
    require_once("Ape.php");
    require_once("Frog.php");

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name ."<br>"; // "buduk"
    echo "legs : " . $kodok->legs ."<br>"; // 4
    echo "cold blooded : " . $kodok->cold_blooded ."<br>"; // "no"
    echo "Jump : ";
    $kodok->jump(); // "hop hop"

    echo "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Name : " . $sungokong->name ."<br>"; // "kera sakti"
    echo "legs : " . $sungokong->legs ."<br>"; // 2
    echo "cold blooded : " . $sungokong->cold_blooded ."<br>"; // "no"
    echo "Yell : ";
    $sungokong->yell(); // "Auooo"
?>