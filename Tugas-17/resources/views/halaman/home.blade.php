@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$nama_depan}} {{$nama_belakang}}</h1>
    <h4>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h4>
@endsection