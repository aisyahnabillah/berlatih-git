<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('halaman.form');
    }

    public function welcome(Request $request) {
        $nama_depan =$request['first_name'];
        $nama_belakang =$request['last_name'];
        return view('halaman.home', compact('nama_depan', 'nama_belakang'));
    }
}
